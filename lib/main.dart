import 'package:flutter/material.dart';
import 'package:test9/dateformat.dart';
import 'package:intl/intl.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'app',
      theme: ThemeData.dark(),
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  final titleImput = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AppBar'),
      ),
      body: Column(
        children: [
          Container(
            height: 600,
            child: ListView(
              scrollDirection: Axis.vertical,
              padding: EdgeInsets.all(5),
              children: [
                CustomContainer(
                  value: 1,
                  height: 300,
                  color: Colors.white,
                ),
                SizedBox(
                  height: 10,
                ),
                CustomContainer(
                  value: 2,
                  width: 100,
                  height: 300,
                  color: Colors.white,
                ),
                SizedBox(
                  height: 10,
                ),
                CustomContainer(
                  value: 3,
                  width: 100,
                  height: 300,
                  color: Colors.white,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}




class CustomContainer extends StatelessWidget {
  double? width;
  double? height;
  Color? color;
  int? value;
  static Map<int, String> numberString = {
    1: 'One',
    2: 'Two',
    3: 'Three',
  };

  CustomContainer({this.value, this.width, this.height, this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(30),
      alignment: Alignment.center,
      width: width,
      height: height,
      child: Text('${numberString[value]}', style: TextStyle(fontSize: 20, color: Colors.black)),
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(35.0),
      ),
    );
  }
}
